# Cagnot'Potes 
Cagnot'Potes est un site de paiement collaboratif réalisé avec Symfony et Materialize

https://jeremy-marconnes.cagnotpotes.tp.simplon-roanne.com/

## Paiement avec l'API Stripe

Le projet exploite Stripe Elements et utilise un système anti fraude.
Stripe est une API moderne qui permet le paiement sur site, à condition d'avoir un certificat SSL

## Développement Symfony 4

Projet découverte de symfony 4, respectant les standards et conventions

## Déploiement en production 100% manuel avec SSL

J'ai déployé mon projet sur une URL et mis en place un certificat SSL sur un serveur Ubuntu hébergé chez OVH.

## Front-end professionnel

J'ai développé ce projet à partir de maquettes HTML professionnelles.
Ce contexte a rendu le projet plus immersif et proche du monde réel.
