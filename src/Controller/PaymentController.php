<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Entity\Campaign;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    

    /**
     * @Route("/charge", name="payment_charge", methods="GET|POST")
     */
    public function charge(Request $request): Response
    {
        $campaign_id =  $request->request->get('campaign_id');

        //Effectuer le paiement, ne pas continuer la fonction si on echoue
        try {
            \Stripe\Stripe::setApiKey('sk_test_mDI79K0ActaXTDMF1vg79sVg');
            $charge = \Stripe\Charge::create([
                'amount' =>(int)$request->request->get("amount") * 100,
                'currency' => 'eur', 
                'source' => $request->request->get('stripeToken') 
            ]);
        }
        catch(\Exception $e) {
            $this->addFlash(
                'error',
                'Le paiement à échoué. Raison : '. $e->getMessage()

            );
            return $this->redirectToRoute('campaign_pay', [
                "id"=> $campaign_id
            ]);
        }



        //Instancier la campaign
        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);

        //Enregistrer un participant
        $participant = new Participant();

        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $participant->setCampaign($campaign);
        
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();


        //Enregistrer le payment qui est dépendant du participant

        $payment = new Payment();

         $payment->setAmount($request->request->get("amount") * 100);

        $payment->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        //Redirection vers la fiche campagne
        return $this->redirectToRoute('campaign_show', [
            "id" =>  $campaign_id
        ]);


    }

    
}
